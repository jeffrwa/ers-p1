package com.reimb.model;

import java.sql.Date;

public class ReimbursementStatus {
	
	short statusId;
	int reimbId;
	Date resolved;
	
	public ReimbursementStatus() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementStatus(short statusId, int reimbId, Date resolved) {
		super();
		this.statusId = statusId;
		this.reimbId = reimbId;
		this.resolved = resolved;
	}

	public short getStatusId() {
		return statusId;
	}

	public void setStatusId(short statusId) {
		this.statusId = statusId;
	}

	public int getReimbId() {
		return reimbId;
	}

	public void setReimbId(int reimbId) {
		this.reimbId = reimbId;
	}

	public Date getResolved() {
		return resolved;
	}

	public void setResolved(Date resolved) {
		this.resolved = resolved;
	}
	
}
