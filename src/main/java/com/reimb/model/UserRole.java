package com.reimb.model;

public class UserRole {
	
	short manager;
	short employee;
	
	public UserRole() {
		// TODO Auto-generated constructor stub
	}

	public UserRole(short manager, short employee) {
		super();
		this.manager = manager;
		this.employee = employee;
	}

	public short getManager() {
		return manager;
	}

	public short getEmployee() {
		return employee;
	}
	
}
