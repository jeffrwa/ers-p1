package com.reimb.model;

public class ReimbursementType {

	short lodging;
	short travel;
	short food;
	short other;
	
	public ReimbursementType() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementType(short lodging, short travel, short food, short other) {
		super();
		this.lodging = lodging;
		this.travel = travel;
		this.food = food;
		this.other = other;
	}

	public short getLodging() {
		return lodging;
	}

	public short getTravel() {
		return travel;
	}

	public short getFood() {
		return food;
	}

	public short getOther() {
		return other;
	}
	
}
