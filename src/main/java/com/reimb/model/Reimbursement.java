package com.reimb.model;

import java.sql.Blob;
import java.sql.Date;
import java.time.LocalDate;

public class Reimbursement {

	int rId;
	Date submitted;
	Date resolved;
	String firstName;
	String lastName;
	String description;
	double amount;
	String type;
	String status;
	//Blob receipt;
	
	public Reimbursement() {
		// TODO Auto-generated constructor stub
	}
	
	public Reimbursement(int rId, Date submitted, Date resolved, String firstName, String lastName, String description,
			double amount, String type, String status) {
		super();
		this.rId = rId;
		this.submitted = submitted;
		this.resolved = resolved;
		this.firstName = firstName;
		this.lastName = lastName;
		this.description = description;
		this.amount = amount;
		this.type = type;
		this.status = status;
	}
	
	public Reimbursement(Date submitted, Date resolved, String firstName, String lastName, String description,
			double amount, String type, String status) {
		super();
		this.submitted = submitted;
		this.resolved = resolved;
		this.firstName = firstName;
		this.lastName = lastName;
		this.description = description;
		this.amount = amount;
		this.type = type;
		this.status = status;
	}
	
	public int getrId() {
		return rId;
	}
	public Date getSubmitted() {
		return submitted;
	}
	public void setSubmitted(Date submitted) {
		this.submitted = submitted;
	}
	public Date getResolved() {
		return resolved;
	}
	public void setResolved(Date resolved) {
		this.resolved = resolved;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "Reimbursement [rId=" + rId + ", submitted=" + submitted + ", resolved=" + resolved + ", firstName="
				+ firstName + ", lastName=" + lastName + ", description=" + description + ", amount=" + amount
				+ ", type=" + type + ", status=" + status + "]";
	}
	
}
