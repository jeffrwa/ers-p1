package com.reimb.dao;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.reimb.LogHelper;
import com.reimb.model.Reimbursement;

public class ReimbursementDaoImpl implements ReimbursementDao {
	
	LogHelper lHelp = new LogHelper();
	
	private ErsDBConnection ersCon;
	
	public ReimbursementDaoImpl() {

	}

	public ReimbursementDaoImpl (ErsDBConnection ersCon) {
		this.ersCon = ersCon;
	}

	@Override
	public void insertReimb(Date submitted, double amount, String description, int author, short statusId, short typeId) {
			
			try (Connection con = ersCon.getDBConnection()) {
				
				String sql = "insert into ers_reimbursement (reimb_submitted, reimb_amount, reimb_description, reimb_author, reimb_status_id, reimb_type_id) values ('" + submitted + "', '" + amount + "', '" + description + "', '" + author + "', '" + typeId + "', '" + statusId + "')";

				Statement statement = con.createStatement();
				int changed = statement.executeUpdate(sql);
				System.out.println("Number of rows changed: " + changed);
				
			} catch (SQLException e) {
				lHelp.callFatalLogger(e);
				//e.printStackTrace();
			}
			
	}
	
	public List<Reimbursement> retrieveReimbursements() {

		List<Reimbursement> reimbList = new ArrayList<>();
		
		try (Connection con = ersCon.getDBConnection()) {
		
			String sql = "select reimb_id, reimb_submitted, reimb_resolved, u.user_first_name, u.user_last_name, reimb_description, reimb_amount, t.reimb_type, s.reimb_status from ers_reimbursement e\r\n"
					+ "	inner join ers_reimbursement_status s on e.reimb_status_id = s.reimb_status_id\r\n"
					+ "	inner join ers_reimbursement_type t on e.reimb_type_id = t.reimb_type_id\r\n"
					+ "	inner join ers_users u on e.reimb_author = u.ers_users_id order by reimb_id";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				reimbList.add(new Reimbursement(rs.getInt(1), rs.getDate(2), rs.getDate(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getDouble(7), rs.getString(8), rs.getString(9)));
			
			}
			
		} catch (SQLException e) {	
			lHelp.callFatalLogger(e);
			//e.printStackTrace();
		}
		
		return reimbList;
	}

	public String toJson() {

		Gson gson = new Gson();
		String jsonArray = gson.toJson(retrieveReimbursements());

			try {
		         FileWriter file = new FileWriter("C:/Users/82217/project-workspace/Project1/src/main/webapp/json/output.json");
		         file.write(jsonArray);
		         file.close();
		      } catch (IOException e) {
		         lHelp.callFatalLogger(e);
		      }
			
			return jsonArray;
		      //System.out.println("JSON file created......");
	}
	
	
}
