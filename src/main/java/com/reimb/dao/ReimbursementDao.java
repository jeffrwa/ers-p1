package com.reimb.dao;

import java.sql.Date;

public interface ReimbursementDao {

	void insertReimb(Date submitted, double amount, String description, int author, short typeId, short statusId);
	
}
