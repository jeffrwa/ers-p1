package com.reimb.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.reimb.LogHelper;

public class ErsDBConnection {

	private final LogHelper lHelp = new LogHelper();
	
	ClassLoader classLoader = getClass().getClassLoader();
	InputStream is;
	Properties p = new Properties();
	
	public ErsDBConnection() {
		is = classLoader.getResourceAsStream("connection.properties");
		try {
			p.load(is);
		} catch (IOException e) {
			lHelp.callFatalLogger(e);
		}
	}

	public Connection getDBConnection() throws SQLException {
		final String URL = p.getProperty("url");
		final String USERNAME = p.getProperty("username");
		final String PASSWORD = p.getProperty("password");
		
	    try {
	    	Class.forName("org.postgresql.Driver");
	    } catch(ClassNotFoundException e) {
	    	lHelp.callFatalLogger(e);
	    }
	      
		return DriverManager.getConnection(URL, USERNAME, PASSWORD);
	}
}
