package com.reimb.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.reimb.LogHelper;

public class ReimbursementStatusDaoImpl {
	
	LogHelper lHelp = new LogHelper();
	
	private ErsDBConnection ersCon;
	
	public ReimbursementStatusDaoImpl() {

	}

	public ReimbursementStatusDaoImpl (ErsDBConnection ersCon) {
		this.ersCon = ersCon;
	}

	public void changeStatus(short statusId, int reimbId, Date resolved) {
		
		try (Connection con = ersCon.getDBConnection()) {
			
			String sql = "update ers_reimbursement set reimb_status_id=? where reimb_id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setShort(1, statusId);
			ps.setInt(2, reimbId);
			ps.executeUpdate();
			
			String sql2 = "update ers_reimbursement set reimb_resolved=? where reimb_id=?";
			PreparedStatement ps2 = con.prepareStatement(sql2);
			ps2.setDate(1, resolved);
			ps2.setInt(2, reimbId);
			ps2.executeUpdate();
		
		} catch (SQLException e) {
			lHelp.callFatalLogger(e);
		}
	}
}
