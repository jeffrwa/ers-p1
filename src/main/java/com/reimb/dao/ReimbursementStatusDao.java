package com.reimb.dao;

import java.sql.Date;

public interface ReimbursementStatusDao {

	void changeStatus(short statusId, int reimbId, Date resolved);
}
