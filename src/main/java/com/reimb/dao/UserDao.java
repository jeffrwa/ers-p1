package com.reimb.dao;

import com.reimb.model.User;

public interface UserDao {

	void insertUser(String userName, String password, String firstName, String lastName, String email, int userRoleId);
	User getByName(String userName);
}
