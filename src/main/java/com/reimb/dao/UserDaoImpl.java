package com.reimb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.reimb.LogHelper;
import com.reimb.model.User;

public class UserDaoImpl implements UserDao {
	
	LogHelper lHelp = new LogHelper();

	private ErsDBConnection ersCon;
	
	public UserDaoImpl() {

	}

	public UserDaoImpl (ErsDBConnection ersCon) {
		this.ersCon = ersCon;
	}
	
	@Override
	public void insertUser(String userName, String password, String firstName, String lastName, String email, int userRoleId) {
		
		try (Connection con = ersCon.getDBConnection()) {
			
			String sql = "{? = call insert_user(?,?,?,?,?,?)}";
			
			CallableStatement cs = con.prepareCall(sql);

			cs.registerOutParameter(1, Types.VARCHAR);
			cs.setString(2, userName);
			cs.setString(3, password);
			cs.setString(4, firstName);
			cs.setString(5, lastName);
			cs.setString(6, email);
			cs.setInt(7, userRoleId);
			cs.execute();
			
			System.out.println(cs.getString(1));
			
		} catch (SQLException e) {
			lHelp.callFatalLogger(e);
		}
	}
	
	@Override
	public User getByName(String userName) {
		try (Connection con = ersCon.getDBConnection()) {
			String sql ="select * from ers_users where ers_username=?";
		
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, userName);
			ResultSet rs = ps.executeQuery();
			User usr = new User();
				
			while (rs.next()) {
				usr = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
			}
			
			return usr;
			
		} catch (SQLException e) {
			lHelp.callFatalLogger(e);
		}
		
		return null;
	}

}
