package com.reimb.controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reimb.dao.ErsDBConnection;
import com.reimb.dao.ReimbursementDaoImpl;
import com.reimb.dao.ReimbursementStatusDaoImpl;
import com.reimb.dao.UserDaoImpl;
import com.reimb.model.User;
import com.reimb.service.ErsService;

public class ErsController {
		
		public static String login(HttpServletRequest req) {
			System.out.println("in controller login");
			if (! req.getMethod().equals("POST")) {
				return "html/unsuccessfullogin.html";
			}
			
			ErsService ersServ = new ErsService(new UserDaoImpl());

			User usr = ersServ.verifyLoginCredentials(req.getParameter("userName"), req.getParameter("password"));

			ReimbursementDaoImpl daoImpl = new ReimbursementDaoImpl(new ErsDBConnection());
			String reimb = daoImpl.toJson();
			
			if (usr == null) {
				return "wrongcreds.change";
			} else {
				req.getSession().setAttribute("currentUser", usr);
				req.getSession().setAttribute("Reimbursements", reimb);
				return "html/home.html";
			}
		}
		
		public static void getSessionErs(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
			User usr = (User)req.getSession().getAttribute("currentUser");
			res.getWriter().write(new ObjectMapper().writeValueAsString(usr));
		}
		
		public static void getReimbursements(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
			String reimb = (String)req.getSession().getAttribute("Reimbursements");
			res.getWriter().write((reimb));
		}
		
		public static String changeStatus(HttpServletRequest req) {
			
			short sId = Short.parseShort(req.getParameter("statusId"));
			System.out.println(sId);
			int rId = Integer.parseInt(req.getParameter("reimbId"));
			System.out.println(rId);
			Date reso = Date.valueOf(req.getParameter("resolved"));
			System.out.println(reso);
		
			ReimbursementStatusDaoImpl rstatus = new ReimbursementStatusDaoImpl(new ErsDBConnection());
		
			rstatus.changeStatus(sId, rId, reso);
			
			return "html/success.html";
			
		}
		
		public static String insertReq(HttpServletRequest req) {
			
			double amo = Double.parseDouble(req.getParameter("amount"));
			System.out.println(amo);
			String desc = req.getParameter("description");
			System.out.println(desc);
			short tId = Short.parseShort(req.getParameter("typeId"));
			System.out.println(tId);
			Date sub = Date.valueOf(req.getParameter("submitted"));
			System.out.println(sub);
			int auth = Integer.parseInt(req.getParameter("author"));
			System.out.println(auth);
			short sId = Short.parseShort(req.getParameter("statusId"));
			
			ReimbursementDaoImpl insData = new ReimbursementDaoImpl(new ErsDBConnection());
			
			insData.insertReimb(sub, amo, desc, auth, tId, sId);
			
			return "html/success.html";

		}
}
