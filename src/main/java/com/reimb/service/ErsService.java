package com.reimb.service;

import com.reimb.dao.ErsDBConnection;
import com.reimb.dao.ReimbursementDaoImpl;
import com.reimb.dao.UserDaoImpl;
import com.reimb.model.User;

public class ErsService {

	private UserDaoImpl usDao;
	private ReimbursementDaoImpl reDao;
	private ErsDBConnection ersCon;
	
	public ErsService() {
		// TODO Auto-generated constructor stub
	}
	
	public ErsService(UserDaoImpl usDao) {
		this.usDao = usDao;
	}
	
	public ErsService(ReimbursementDaoImpl reDao) {
		this.reDao = reDao;
	}
	
	public ErsService(ErsDBConnection ersCon) {
		this.ersCon = ersCon;
	}
	
	public User verifyLoginCredentials(String userName, String password) {
		
		ErsDBConnection ers = new ErsDBConnection();
		UserDaoImpl uDao = new UserDaoImpl(ers);

		User usr = uDao.getByName(userName);
		System.out.println(usr);
		if (usr.getUserName() != null) {
			if (usr.getPassword().equals(password)) {
				return usr;
			}
		}
		
		return null;
	}

}
