package com.reimb;

import org.apache.log4j.Logger;

public class LogHelper {
	public static final Logger log = Logger.getLogger(MainDriver.class);

	public void callFatalLogger(Object e) {
		log.fatal("Fatal: " + e);	
	}
	
	public void callWarnLogger(Object e) {
		log.warn("Warn: " + e);
	}
}
