package com.reimb;

import java.sql.SQLException;
import java.time.LocalDate;

import com.reimb.dao.ErsDBConnection;
import com.reimb.dao.ReimbursementDaoImpl;
import com.reimb.dao.UserDaoImpl;
import com.reimb.service.ErsService;

public class MainDriver {

	public static void main(String[] args) throws SQLException {
		
		ErsDBConnection con = new ErsDBConnection();
		UserDaoImpl uDao = new UserDaoImpl(con);
		ReimbursementDaoImpl rDao = new ReimbursementDaoImpl(con);
		
		//uDao.insertUser("bigboss", "bigboss", "John", "Wick", "jw@thecontinental.com", 1);
		//uDao.insertUser("usa", "usa", "Ivanka", "Trump", "ivanka@trump.com", 2);
		//uDao.insertUser("zen", "zen", "Lao", "Tzu", "yinyang@taoism.com", 2);
		//uDao.insertUser("alien", "alien", "Space", "Monkey", "unknown@outerspace.com", 2);
		// localDate = LocalDate.now();
		//System.out.println(localDate);
		//rDao.insertReimb(2000, localDate, "Luxury Suite", 2, 3, 1);
		
		//ErsService ers = new ErsService(uDao);
		//ers.verifyLoginCredentials("bigboss", "bigboss");

		//System.out.println(uDao.getByName("bigboss"));
		
		
		//System.out.println(rDao.retrieveReimbursements());
		
		//rDao.toJson();
	}

}
