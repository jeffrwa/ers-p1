package com.reimb.servlets;

import javax.servlet.http.HttpServletRequest;

import com.reimb.controller.ErsController;

public class ViewDispatcher {

	public static String process(HttpServletRequest req) {
		
		switch (req.getRequestURI()) {
		
			case "/Project1/login.change":
				System.out.println("in login.change dispatcher");
				return ErsController.login(req);
				
			default:
				System.out.println("in default");
				return "html/unsuccessfullogin.html";
		}
	}
}
