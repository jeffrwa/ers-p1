package com.reimb.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reimb.controller.ErsController;
import com.reimb.model.Reimbursement;
import com.reimb.model.User;

public class JSONDispatcherServlet {
		
		public static void process(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
			
			switch(req.getRequestURI()) {
			
			 case "/Project1/getsessioners.json":
				 ErsController.getSessionErs(req, res);
				 break;
				 
			 case "/Project1/getreimbursements.json":
				 ErsController.getReimbursements(req, res);
				 break;
				 
			default:
				res.getWriter().write(new ObjectMapper().writeValueAsString(new User()));
				res.getWriter().write(new ObjectMapper().writeValueAsString(new Reimbursement()));
			
			}
		}

}

