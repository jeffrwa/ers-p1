package com.reimb.servlets;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.reimb.controller.ErsController;
import com.reimb.dao.ReimbursementStatusDaoImpl;


public class StatusDispatcher {
	
	public static String process(HttpServletRequest req) {
		
		switch (req.getRequestURI()) {
		
		case "/Project1/changestatus":
			
			System.out.println("status");
			return ErsController.changeStatus(req);

		case "/Project1/insertreq":
			
			System.out.println("insert");
			return ErsController.insertReq(req);
			
		default:
			
			System.out.println("default");
			return "html/home.html";
		}
	
	}
		
}
