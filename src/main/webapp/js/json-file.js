window.onload = function() {
	getTableData();
}

function getTableData() {
	
    let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function() {

		if (xhttp.readyState == 4 && xhttp.status == 200) {
			let reimb = JSON.parse(xhttp.responseText);
			console.log(reimb);
			
       		let table = document.createElement("table");

			let row = table.insertRow(0);
		
			row.insertCell(0).outerHTML = "<th>Submitted</th>";
			row.insertCell(1).outerHTML = "<th>Resolved</th>";
			row.insertCell(2).outerHTML = "<th>First Name</th>";
			row.insertCell(3).outerHTML = "<th>Last Name</th>";
			row.insertCell(4).outerHTML = "<th>Description</th>";
			row.insertCell(5).outerHTML = "<th>Amount</th>";
			row.insertCell(6).outerHTML = "<th>Type</th>";
			row.insertCell(7).outerHTML = "<th>Status</th>";


	        let divContainer = document.getElementById("showData");
    	    divContainer.innerHTML = "";
        	divContainer.appendChild(table);

			for (let i = 0 ; i < reimb.length; i++) {
				
				let tr = table.insertRow(-1);
				
				let sub = reimb[i].submitted;
				let res = reimb[i].resolved;
				let fn = reimb[i].firstName;
				let ln = reimb[i].lastName;
				let des = reimb[i].description;
				let amo = reimb[i].amount;
				let ty = reimb[i].type;
				let sta = reimb[i].status;
				
				let cell0 = tr.insertCell(-1);
				cell0.innerHTML = sub;
				let cell1 = tr.insertCell(-1);
				cell1.innerHTML = res;
				let cell2 = tr.insertCell(-1);
				cell2.innerHTML = fn; 
				let cell3 = tr.insertCell(-1);
				cell3.innerHTML = ln;
				let cell4 = tr.insertCell(-1);
				cell4.innerHTML = des; 
				let cell5 = tr.insertCell(-1);
				cell5.innerHTML = amo;
				let cell6 = tr.insertCell(-1);
				cell6.innerHTML = ty; 
				let cell7 = tr.insertCell(-1);
				cell7.innerHTML = sta;
			}
		}
			
	}
	
	xhttp.open("GET", "http://localhost:8080/Project1/getreimbursements.json");
	xhttp.send();
	
}	