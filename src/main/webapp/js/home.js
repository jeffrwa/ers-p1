window.onload = function() {
	getSessErs();
}

let fname;
let lname;

function getSessErs() {
	let xhttp = new XMLHttpRequest();
	
	
	xhttp.onreadystatechange = function() {
		
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			
			let user = JSON.parse(xhttp.responseText);
			/*console.log(user);
			console.log(user.firstName);*/
			document.getElementById("welcomeHeader").innerText=`Welcome ${user.firstName} ${user.lastName}`;
			document.getElementById("author").value = user.userId;
			fname = user.firstName;
			lname = user.lastName;
			/*console.log(fname);
			console.log(lname);*/
			
			if (`${user.userRoleId}` == 1) {
				
				let btn = document.createElement("button");
				btn.innerHTML = "List Reimbursements";
				document.body.appendChild(btn);
				btn.addEventListener("click", getTableData);
				btn.className = "btn btn-dark";
				
				let btn2 = document.createElement("button");
				btn2.innerHTML = "Modify Reimbursements";
				document.body.appendChild(btn2);
				btn2.className = "btn btn-dark";
				btn2.onclick = function(){toggle("hiddenForm")};
				
				let iput = document.createElement("input");
				document.body.append(iput);
				iput.placeholder = "Filter Reimbursements";
				iput.setAttribute("id", "search");
				iput.setAttribute("class", "form-control");
	
				iput.onkeyup = function() {
		
					input = document.getElementById("search");
  					filter = input.value.toUpperCase();
					let tr = document.getElementsByTagName("tr");
		
					for (i = 1; i < tr.length; i++) {
			
    					tr[i].style.display = "none";

						td = tr[i].getElementsByTagName("td");
    					for (var j = 0; j < td.length; j++) {
      						cell = tr[i].getElementsByTagName("td")[j];
      						
							if (cell) {
        						if (cell.innerHTML.toUpperCase().indexOf(filter) > -1) {
          						tr[i].style.display = "";
          						break;
        						} 
     						}
   		 				}
					}
				};	
				
			}
			
			if (`${user.userRoleId}` == 2) {
				
				/*console.log(fname);
				console.log(lname);*/
				
				let btnEmp = document.createElement("button");
				btnEmp.innerHTML = "List My Reimbursement Requests";
				document.body.appendChild(btnEmp);
				btnEmp.addEventListener("click", getEmpData);
				btnEmp.className = "btn btn-dark";
				
				let btnEmp2 = document.createElement("button");
				btnEmp2.innerHTML = "Add Reimbursement Request";
				document.body.appendChild(btnEmp2);
				btnEmp2.className = "btn btn-dark";
				btnEmp2.onclick = function(){toggle("hiddenReqForm")};
  					
			}
		}
	}
	
	xhttp.open("GET", "http://localhost:8080/Project1/getsessioners.json");

	xhttp.send();
	
function getTableData() {
	
    let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function() {

		if (xhttp.readyState == 4 && xhttp.status == 200) {
			let reimb = JSON.parse(xhttp.responseText);
			/*console.log(reimb);*/
			
       		let table = document.createElement("table");
		
			let row = table.insertRow(0);
		
			row.insertCell(0).outerHTML = "<th>ID</th>"
			row.insertCell(1).outerHTML = "<th>Submitted</th>";
			row.insertCell(2).outerHTML = "<th>Resolved</th>";
			row.insertCell(3).outerHTML = "<th>First Name</th>";
			row.insertCell(4).outerHTML = "<th>Last Name</th>";
			row.insertCell(5).outerHTML = "<th>Description</th>";
			row.insertCell(6).outerHTML = "<th>Amount</th>";
			row.insertCell(7).outerHTML = "<th>Type</th>";
			row.insertCell(8).outerHTML = "<th>Status</th>";

	        let divContainer = document.getElementById("showData");
    	    divContainer.innerHTML = "";
        	divContainer.appendChild(table);

			for (let i = 0 ; i < reimb.length; i++) {
				
				let tr = table.insertRow(-1);
				
				let rid = reimb[i].rId;
				let sub = reimb[i].submitted;
				let res = reimb[i].resolved;
				let fn = reimb[i].firstName;
				let ln = reimb[i].lastName;
				let des = reimb[i].description;
				let amo = reimb[i].amount;
				let ty = reimb[i].type;
				let sta = reimb[i].status;
				
				
				let cell0 = tr.insertCell(-1);
				cell0.innerHTML = rid;
				let cell1 = tr.insertCell(-1);
				cell1.innerHTML = sub;
				let cell2 = tr.insertCell(-1);
				cell2.innerHTML = res;
				let cell3 = tr.insertCell(-1);
				cell3.innerHTML = fn; 
				let cell4 = tr.insertCell(-1);
				cell4.innerHTML = ln;
				let cell5 = tr.insertCell(-1);
				cell5.innerHTML = des; 
				let cell6 = tr.insertCell(-1);
				cell6.innerHTML = amo;
				let cell7 = tr.insertCell(-1);
				cell7.innerHTML = ty; 
				let cell8 = tr.insertCell(-1);
				cell8.innerHTML = sta;
			}
		}
			
	}
	
	xhttp.open("GET", "http://localhost:8080/Project1/getreimbursements.json");
	xhttp.send();
	
	
	
}

function getEmpData() {
	
    let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function() {

		if (xhttp.readyState == 4 && xhttp.status == 200) {
			let reimb = JSON.parse(xhttp.responseText);
			/*console.log(reimb);*/
			/*console.log(fname);
			console.log(lname);*/
			
			let table = document.createElement("table");
		
			let row = table.insertRow(0);
		
			row.insertCell(0).outerHTML = "<th>ID</th>"
			row.insertCell(1).outerHTML = "<th>Submitted</th>";
			row.insertCell(2).outerHTML = "<th>Resolved</th>";
			row.insertCell(3).outerHTML = "<th>Description</th>";
			row.insertCell(4).outerHTML = "<th>Amount</th>";
			row.insertCell(5).outerHTML = "<th>Type</th>";
			row.insertCell(6).outerHTML = "<th>Status</th>";

	        let divContainer = document.getElementById("showData");
    	    divContainer.innerHTML = "";
        	divContainer.appendChild(table);

			for (let i = 0 ; i < reimb.length; i++) {
				
				if (reimb[i].firstName == fname && reimb[i].lastName == lname) {
				
					/*console.log(reimb[i].firstName);
					console.log(reimb[i].lastName);*/
				
					let tr = table.insertRow(-1);
				
					let rid = reimb[i].rId;
					let sub = reimb[i].submitted;
					let res = reimb[i].resolved;
					let des = reimb[i].description;
					let amo = reimb[i].amount;
					let ty = reimb[i].type;
					let sta = reimb[i].status;
				
					let cell0 = tr.insertCell(-1);
					cell0.innerHTML = rid;
					let cell1 = tr.insertCell(-1);
					cell1.innerHTML = sub;
					let cell2 = tr.insertCell(-1);
					cell2.innerHTML = res;
					let cell3 = tr.insertCell(-1);
					cell3.innerHTML = des; 
					let cell4 = tr.insertCell(-1);
					cell4.innerHTML = amo;
					let cell5 = tr.insertCell(-1);
					cell5.innerHTML = ty; 
					let cell6 = tr.insertCell(-1);
					cell6.innerHTML = sta;
				}
			}
		}
			
	}
	
	xhttp.open("GET", "http://localhost:8080/Project1/getreimbursements.json");
	xhttp.send();
}

function toggle(fid) {
	/*console.log(fid);*/
	if (document.getElementById(fid).style.display !== "none") {
    	document.getElementById(fid).style.display = "none";
	} else {
		document.getElementById(fid).style.display = "block";
 	}
}

}